/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rozsiritelne_hesovanie;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.BitSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Robo
 */
public class RozsiritelneHesovanie {
    private static final int MAX_HLBKA_SUBORU = 20;//2^20 

    private int[] adresar;
    private int D;
    private int maxPocetZaznamov;
    private final String FILE;
    private String FILE_P;
    private Blok aktBlok;
    private Zaznam aktZaznam;
    private boolean preplnovanie;
    private int aktAdresaPrepln;
    private int maxAdresa;
    private RandomAccessFile raf;
    private RandomAccessFile raf_P;
    
    public RozsiritelneHesovanie(String nazovSuboru, int maxPocZazn, Blok akt) {
        D = 1;
        adresar = new int[2];
        FILE = nazovSuboru;
        FILE_P = nazovSuboru.substring(0,nazovSuboru.length()-4)+"_preplnovaci.bin";
        maxPocetZaznamov = maxPocZazn;
        aktBlok = akt;
        aktZaznam = aktBlok.getZaznamBloku();
        preplnovanie = false;
        aktAdresaPrepln = 0;
        maxAdresa = 0;
    }
    
    //
    public void init() {
        otvorSubor();
        vlozPrazdny(aktBlok);
        zavriSubor();
    }
    
    //metoda pre vlozenie zaznamu
    public boolean insert (Zaznam z) {
        while (true) {
                int index = vypocitajHash(z); 
                Blok b = new Blok(getMaxPocetZaznamov(),aktZaznam);
                byte[] pole = nacitajBlokZoSuboru(getAdresar()[index],b.getSize());
                b.nacitajBlok(pole);
                if (b.blokJePlny()) {
                    if (b.getD() == getD()) {
                            //ak som vyuzil vsetky bity hesu a blok je plny a d=D - preplnovacie
                            if(preplnovanie) {
                                 
                            } else {
                                if(!zdvojnasobAdresar()) {
                                    //ak mi nedovoli zdvojnasobit adresar, lebo by prekrocil max dlzku tiez preplnovat, aj ked nevyuzivam vsetky bity
                                    //max dlzka je mensia ako pocet bitov hesu - vzdy budem preplnovat iba sem
                                    return preplnenieBloku(b,z,index);
                                    
                                }
                                index = 2 * index;
                            }
                    }
                    rozdelenieBloku(b,index);//split - vytvorenie noveho bloku
                }else {   
                    if(b.vlozZaznam(z) == -1)
                        return false;
                    zapisBlokDoSuboru(getAdresar()[index], b);
                    return true;
                }
        }
    }
    
    //metoda pre najdenie zaznamu
    public Zaznam search(Zaznam z) {
        otvorSubor();
        Zaznam res=null;
        int adresa = vypocitajHash(z);
        Blok b = new Blok(getMaxPocetZaznamov(),aktZaznam);
        byte[] pole = nacitajBlokZoSuboru(getAdresar()[adresa],b.getSize());
        b.vymazZaznamy();
        b.nacitajBlok(pole);
        res = b.najdiZaznam(z);
        if(res == null) {
            //nenaslo hladaj v preplnujucich dokym nenajdes zaznam alebo dokym existuje preplnujuci
            while(b.getAdresaPrepl() != -1) {
                pole = nacitajBlokZPreplnSuboru(b.getAdresaPrepl(),b.getSize());
                b.vymazZaznamy();
                b.nacitajBlok(pole);
                res = b.najdiZaznam(z);
                if(res != null)
                    return res;
            }
        }
        zavriSubor();
        return res;
    }
    
    //metoda pre zmenu zaznamu
    public boolean update(Zaznam novyZ) {
        int a;
        otvorSubor();
        boolean res=false;
        int adresa = vypocitajHash(novyZ);
        Blok b = new Blok(getMaxPocetZaznamov(),aktZaznam);
        byte[] pole = nacitajBlokZoSuboru(getAdresar()[adresa],b.getSize());
        b.vymazZaznamy();
        b.nacitajBlok(pole);
        res = b.prepisZaznam(novyZ);
        if(!res) {
            //nenaslo hladaj v preplnujucich dokym nenajdes zaznam alebo dokym existuje preplnujuci
            while(b.getAdresaPrepl() != -1) {
                pole = nacitajBlokZPreplnSuboru(b.getAdresaPrepl(),b.getSize());
                a=b.getAdresaPrepl();
                b.vymazZaznamy();
                b.nacitajBlok(pole);
                res = b.prepisZaznam(novyZ);
                if(res) {
                    zapisBlokDoPreplnSuboru(a, b);
                    zavriSubor();
                    return res;
                }
            }
        } else {
            zapisBlokDoSuboru(adresar[adresa], b);
            zavriSubor();
            return res;
        }
        zavriSubor();
        return res;
    }

    private int vypocitajHash(Zaznam z) {
        int hash = Math.abs(z.getHash());
        int index = 0;
        BitSet b = new BitSet();
        while (hash != 0) {
            boolean boolValue;
            if(hash%2 == 1) {
                boolValue = true;
            } else {
                boolValue = false;
            }
            b.set(index, boolValue);
            hash = hash / 2;
            index++;
        }
        BitSet res = new BitSet();
        for(int i=0;i<D;i++) {
            res.set(i,b.get(b.length()-i));
        }
        int adresa;
        if(res.toLongArray().length>0)
            adresa = (int) res.toLongArray()[0];
        else {
            adresa = 0;
        }
        return adresa;
//        String binaryHash = Integer.toBinaryString(hash);
//        String reversedBinaryHash = new StringBuilder(binaryHash).reverse().toString();
//        //aby mi neprekrocilo pocet bitov z hashu - preplnovacie....
//        int endIndex = this.getD();
//        if(endIndex >= reversedBinaryHash.length()) {
//            endIndex = reversedBinaryHash.length();
//            preplnovanie = true;
//        }
//        return Integer.parseInt(reversedBinaryHash.substring(0, endIndex),2);
    }

    //metoda nacita pole bytov zo suboru
    public byte[] nacitajBlokZoSuboru(int adresa,int size) {
        byte[] pole = new byte[size];
        try {
            raf.seek(adresa);
            raf.readFully(pole);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RozsiritelneHesovanie.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RozsiritelneHesovanie.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pole;
    }
    
    //metoda nacita pole bytov z preplnujuceho suboru
    public byte[] nacitajBlokZPreplnSuboru(int adresa,int size) {
        byte[] pole = new byte[size];
        try {
            raf_P.seek(adresa);
            raf_P.readFully(pole);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RozsiritelneHesovanie.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RozsiritelneHesovanie.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pole;
    }
    
    public void otvorSubor() {
        try {
            raf  = new RandomAccessFile(FILE, "rw");
            raf_P = new RandomAccessFile(FILE_P, "rw");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RozsiritelneHesovanie.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void zavriSubor() {
        try {
            raf.close();
            raf_P.close();
        } catch (FileNotFoundException ex) {
            
        }   catch (IOException ex) {
            Logger.getLogger(RozsiritelneHesovanie.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
                
    //metoda zapise blok ako pole bytov do suboru
    public void zapisBlokDoSuboru(int pozicia, Blok b ) {
        try {
            raf.seek(pozicia);
            b.getByteArray();
            raf.write(b.getByteArray());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RozsiritelneHesovanie.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RozsiritelneHesovanie.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //metoda zapise blok ako pole bytov do preplnujuceho suboru
    public void zapisBlokDoPreplnSuboru(int pozicia, Blok b ) {
        try {
            raf_P.seek(pozicia);
            b.getByteArray();
            raf_P.write(b.getByteArray());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RozsiritelneHesovanie.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RozsiritelneHesovanie.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // pri vytvoreni automaticy vlozi 2 bloky
    private void vlozPrazdny(Blok b) {
        adresar[0] = 0;
        adresar[1] = b.getSize();
        maxAdresa = b.getSize();
        zapisBlokDoSuboru(getAdresar()[0], b);
        zapisBlokDoSuboru(getAdresar()[1], b);
    }

    //metoda, ktora zdvojnasobi dlzku adresara
    private boolean zdvojnasobAdresar() {
        //aby nezvacsovalo do nekonecna
        if(getD()+1 >= MAX_HLBKA_SUBORU) {
            return false;
        }
        setD(getD() +1);
        int[] novyAdresar = new int[2*getAdresar().length];
        for(int i=0;i<getAdresar().length;i++) {
            novyAdresar[2*i] = getAdresar()[i];
            novyAdresar[2*i+1] = getAdresar()[i];
        }
        setAdresar(novyAdresar);
        return true;
    }

    //metoda, ktora rozdeli blok na dva
    private void rozdelenieBloku(Blok b,int index) {
        Blok novy = new Blok(getMaxPocetZaznamov(), b.getZaznamBloku());
        //pocet indexov v adresary, ktore ukazuju na blok, ktory idem delit
        int pocetIndexov =(int) Math.pow(2,(getD() -b.getD()));
        //prva polovica bude ukazovat na uz existujuci, druha polovica na novy blok
        int adresaStara = getAdresar()[index];
        int adresaNova = maxAdresa +b.getSize();
        maxAdresa = adresaNova;
        
//        for(int i=pocetIndexov/2;i<pocetIndexov;i++) {//pocetIndexov
//            if(adresar.length <= (index+i)) {
//                System.out.println(",,,");
//                break;
//            }
//            adresar[index+i] = adresaNova;
//        }
        
        for(int i=0;i<getAdresar().length;i++) {
            if(getAdresar()[i] == getAdresar()[index]) {
                for(int j=0;j<pocetIndexov/2;j++) {
                    adresar[i+j+pocetIndexov/2] = adresaNova;
                }
                break;
            }
        }
        
        
        Zaznam[] z = b.getZaznamy();
        b.vymazZaznamy();
        b.zvacsiHlbku();
        novy.setD(b.getD());
        zapisBlokDoSuboru(adresaNova, novy);//novy prazdny blok na novej adrese
        zapisBlokDoSuboru(adresaStara, b);// stary blok prepise na prazdny adresar[index]
        for(int i=0;i<z.length;i++)
            insert(z[i]);
    }
    
    public void vypisAdresar() {
        System.out.println("adresar:");
        for(int i=0;i<getAdresar().length;i++) {
            System.out.print(getAdresar()[i]+", ");
        }
    }

    /**
     * @param adresar the adresar to set
     */
    public void setAdresar(int[] adresar) {
        this.adresar = adresar;
    }

    /**
     * @param D the D to set
     */
    public void setD(int D) {
        this.D = D;
    }

    /**
     * @param maxPocetZaznamov the maxPocetZaznamov to set
     */
    public void setMaxPocetZaznamov(int maxPocetZaznamov) {
        this.maxPocetZaznamov = maxPocetZaznamov;
    }

    /**
     * @return the adresar
     */
    public int[] getAdresar() {
        return adresar;
    }

    /**
     * @return the D
     */
    public int getD() {
        return D;
    }

    /**
     * @return the maxPocetZaznamov
     */
    public int getMaxPocetZaznamov() {
        return maxPocetZaznamov;
    }

    private boolean preplnenieBloku(Blok b, Zaznam z,int index) {
        Blok novy;
        byte [] pole;
        Blok pomBlok = null;
        int vysl;
        if(b.getAdresaPrepl() == -1) {
                novy = new Blok(maxPocetZaznamov,b.getZaznamBloku());
                b.setAdresaPrepl(aktAdresaPrepln);
                zapisBlokDoSuboru(adresar[index], b);
                novy.vlozZaznam(z);
                zapisBlokDoPreplnSuboru(aktAdresaPrepln, novy);
                aktAdresaPrepln += novy.getSize();
                return true;
        }
        while(true) {
            //ak neexistuje preplnovaci vytvor ho
            if(b.getAdresaPrepl() == -1) {
                novy = new Blok(maxPocetZaznamov,b.getZaznamBloku());
                b.setAdresaPrepl(aktAdresaPrepln);
                novy.vlozZaznam(z);
                zapisBlokDoPreplnSuboru(index, pomBlok);//!!!!!
                zapisBlokDoPreplnSuboru(aktAdresaPrepln, novy);
                aktAdresaPrepln += novy.getSize();
                return true;
            }
            //inak existuje preplnovaci hladaj v zretazeni prreplnujuci s volnym miestom
            if(b.getAdresaPrepl() != -1) {
                pole = nacitajBlokZPreplnSuboru(b.getAdresaPrepl(), b.getSize());
                index = b.getAdresaPrepl();//zapamatam si adresu aktualneho, aby ked mu vytvorim dalsi zretazeny tak mu mozem prepisat v subore 
                pomBlok = b;
                b.vymazZaznamy();
                b.nacitajBlok(pole);
                vysl = b.vlozZaznam(z);
                if(vysl == 0) {
                    return false;
                }
                //vlozilo zaznam
                if(vysl == 1) {
                    zapisBlokDoPreplnSuboru(index, b);
                    return true;
                }
            }
        }
    }
    
    public String sekvencnyVypis() {
        otvorSubor();
        String pom ="";
        byte[] pole;
        int count;
        int adresa;
        Zaznam [] zazn;
        for(int i=0;i<adresar.length;i++) {
            count =0 ;
            Blok b = new Blok(getMaxPocetZaznamov(),aktZaznam);
            pole = nacitajBlokZoSuboru(adresar[i],b.getSize());
            b.nacitajBlok(pole);
            zazn = b.getZaznamy();
            pom +="Zaznamy v "+(i+1)+". bloku na adrese "+adresar[i]+". Hlbka bloku: "+b.getD()+". Adresa preplnujuceho suboru: "+b.getAdresaPrepl()+"\n";
            for(int j=0;j<zazn.length;j++) {
                if(zazn[j] != null)
                    pom += zazn[j].toString()+"\n";
            }
            while(b.getAdresaPrepl() != -1) {
                count++;
                pole = nacitajBlokZPreplnSuboru(b.getAdresaPrepl(), b.getSize());
                adresa = b.getAdresaPrepl();
                b.vymazZaznamy();
                b.nacitajBlok(pole);
                for(int l=0;l<count;l++) {
                    pom+="\t";
                }
                pom += "Zaznamy v preplnujucom bloku "+count+" na adrese preplnujuceho bloku "+adresa+". Adresa nasledujuceho prepln. bloku:"+b.getAdresaPrepl()+"\n";
                zazn = null;
                zazn = b.getZaznamy();
                for(int k=0;k<zazn.length;k++) {
                    if(zazn[k] != null) {
                        for(int l=0;l<count;l++) {
                            pom+="\t";
                        }
                        pom += zazn[k].toString()+"\n";
                    }
                }
            }
        }
        zavriSubor();
        return pom;
    }
}
