/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rozsiritelne_hesovanie;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author Robo
 */
public class Blok {
    private Zaznam[] zaznamy;
    private int d;
    private int adresaPrepl;
    private int maxPocetZaznamov;
    private Zaznam zaznamBloku;
    private byte[] hlavicka;//hlbka bloku
    private byte[] hlavicka2;//adresa preplnujuceho
    
    public Blok(int maxPocZ,Zaznam z) {
        d = 1;
        adresaPrepl = -1;
        maxPocetZaznamov = maxPocZ;
        zaznamy = new Zaznam[maxPocetZaznamov];
        zaznamBloku = z;
        hlavicka = new byte[4];
        hlavicka2 = new byte[4];
    }
    
    //metoda na vlozenie zaznamu o bloku
    //1 vlozilo, 0 uz taky mam, -1 nevlozilo nie je miesto

    /**
     * metoda na vlozenie zaznamu o bloku
     * @param z
     * @return vlozilo 1,zaznam uz existuje 0, nevlozilo -1
     */
    public int vlozZaznam(Zaznam z) {
        for(int i=0;i<getMaxPocetZaznamov();i++) {
            if(zaznamy[i] != null && z != null) {
                if(zaznamy[i].compare(z)) {
                    System.out.println("uz mam prvom s danym klucom");
                    return 0;
                }
            } 
            if(getZaznamy()[i] ==  null && z != null ) {
                zaznamy[i] = z;
                return 1;
            }
        }
        return -1;
    }
    
    //metoda nacita blok z pola bytov
    public void nacitajBlok(byte[] pole) {
        System.arraycopy(pole, 0, hlavicka, 0, hlavicka.length);
        System.arraycopy(pole, hlavicka.length, hlavicka2, 0, hlavicka2.length);
        ByteArrayInputStream hlpByteArrayInputStream = new ByteArrayInputStream(pole);
        DataInputStream hlpInStream = new DataInputStream(hlpByteArrayInputStream);
        try {
            this.d = hlpInStream.readInt();
            this.adresaPrepl = hlpInStream.readInt();
        } catch (IOException e) {
            throw new IllegalStateException("Error during conversion from byte array.");
        }
        byte[] pom = new byte[getZaznamBloku().getDlzkaZaznamu()];
        for(int i=0;i<getMaxPocetZaznamov();i++) {
            System.arraycopy(pole, hlavicka.length + hlavicka2.length + i*getZaznamBloku().getDlzkaZaznamu(), pom, 0, getZaznamBloku().getDlzkaZaznamu());
            zaznamBloku.FromByteArray(pom);
            vlozZaznam(getZaznamBloku().FromByteArray(pom));//z
        }
    }
    
    //najde zaznam v zaznamoch bloku
    public Zaznam najdiZaznam(Zaznam z) {
        for(int i=0;i<getZaznamy().length;i++) {
            if(getZaznamy()[i] != null) {
                if(getZaznamy()[i].compare(z))
                    return getZaznamy()[i];
            }
        }
        return null;
    }

    boolean prepisZaznam(Zaznam novyZ) {
        for(int i=0;i<getZaznamy().length;i++) {
            if(getZaznamy()[i] != null) {
                if(getZaznamy()[i].compare(novyZ)) {
                    System.out.println("zmena");
                    zaznamy[i] = novyZ;
                    return true;
                }
            }
        }
        return false;
    }
    
    //metoda, kt vrati true ak je blok plny, inak vrati true
    public boolean blokJePlny() {
        for(int i=0;i<getMaxPocetZaznamov();i++) {
            if(getZaznamy()[i] == null)
                return false;
            if(!zaznamy[i].isPlatny()) {
                return false;
            }
        }
        return true;
    }
    
    public void vymazZaznamy() {
        zaznamy = new Zaznam[maxPocetZaznamov];
        hlavicka = new byte[4];
        hlavicka2 = new byte[4];
    }
    
    public int getSize() {
        return hlavicka.length + hlavicka2.length + getMaxPocetZaznamov() * getZaznamBloku().getDlzkaZaznamu() ;
    }
    
    //metoda ktora zapise blok do pola bytov
    public byte[] getByteArray() {     
        ByteArrayOutputStream hlpByteArrayOutputStream= new ByteArrayOutputStream();
        DataOutputStream hlpOutStream = new DataOutputStream(hlpByteArrayOutputStream);
        try{
            hlpOutStream.writeInt(this.d);
            hlpOutStream.writeInt(this.adresaPrepl);
        }catch (IOException e){
            throw new IllegalStateException("Error during conversion to byte array.");
        }
        byte[] hlavicky = hlpByteArrayOutputStream.toByteArray();
        byte[] pole = new byte[hlavicky.length + getMaxPocetZaznamov() * getZaznamBloku().getDlzkaZaznamu()];
        System.arraycopy(hlavicky, 0, pole, 0, hlavicky.length);
        for(int i=0;i<getMaxPocetZaznamov();i++) {
            if(getZaznamy()[i] != null) {
                System.arraycopy(getZaznamy()[i].ToByteArray(), 0, pole, hlavicky.length + i*getZaznamBloku().getDlzkaZaznamu(), getZaznamBloku().getDlzkaZaznamu());
            }
        }
        return pole;
    }
    
    public void vypisZaznamy() {
        System.out.println("VYPIS: ");
        for(int i=0;i<getMaxPocetZaznamov();i++) {
            if(getZaznamy()[i] != null) {
                System.out.println((i+1)+". "+getZaznamy()[i].toString());
            }
        }
        System.out.println("KONIEC VYPISU: ");
    }

    /**
     * @return the maxPocetZaznamov
     */
    public int getMaxPocetZaznamov() {
        return maxPocetZaznamov;
    }

    /**
     * @return the d
     */
    public int getD() {
        return d;
    }

    /**
     *
     */
    public void zvacsiHlbku() {
        this.setD(this.getD() + 1);
    }

    /**
     * @param d the d to set
     */
    public void setD(int d) {
        this.d = d;
    }

    /**
     * @return the zaznamy
     */
    public Zaznam[] getZaznamy() {
        return zaznamy;
    }

    /**
     * @return the zaznamBloku
     */
    public Zaznam getZaznamBloku() {
        return zaznamBloku;
    }

    /**
     * @return the adresaPrepl
     */
    public int getAdresaPrepl() {
        return adresaPrepl;
    }

    /**
     * @param adresaPrepl the adresaPrepl to set
     */
    public void setAdresaPrepl(int adresaPrepl) {
        this.adresaPrepl = adresaPrepl;
    }
}
