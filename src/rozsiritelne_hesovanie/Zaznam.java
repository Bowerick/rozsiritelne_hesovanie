/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rozsiritelne_hesovanie;

import java.io.DataInputStream;
import java.io.IOException;

/**
 *
 * @author Robo
 */
public abstract class Zaznam {
    private static final char SPECIALNY_ZNAK = '*';
    
    private boolean platny;
    
    public Zaznam() {
        
    }
    
    public abstract int getHash();
    
    //pole bytov prevedie na zaznam
    public abstract Zaznam FromByteArray(byte[] paArray);
    
    //zo zoznamu spravi pole bytov
    public abstract byte[] ToByteArray();
    
    //metoda vrati dlzku zaznamu
    public abstract int getDlzkaZaznamu();
    
    //metoda pre porovnanie dvoch zaznamov
    public abstract boolean compare(Zaznam z);

    /**
     *
     * @param hlpInStream
     * @param MAX_DLZKA
     * @return vrati povodne slovo 
     */
    public String getSlovo(DataInputStream hlpInStream, int MAX_DLZKA) {
        String slovo= "";
        char c;
        for(int i=0;i<MAX_DLZKA;i++) {
            try {
                c = hlpInStream.readChar();
                if(c != SPECIALNY_ZNAK)
                    slovo += c;
            } catch (IOException ex) {
            }
        }
        return slovo;
    }
    
    /**
     *
     * @param src
     * @param MAX_DLZKA
     * @return ak je retazec kratsi doplni ho specialnymi znakmi,ak je dlhsi oreze ho
     */
    public String getFullString(String src,int MAX_DLZKA) {
        if(src.length() < MAX_DLZKA) {
            String pom = "";
            for(int i=0;i<MAX_DLZKA;i++) {
                if(i<src.length()) {
                    pom += src.charAt(i);
                } else
                    pom += SPECIALNY_ZNAK;
            }
            return pom;
        } else {
            if(src.length() == MAX_DLZKA) {
                return src;
            } else {
                //exclusive - posledny char nevezme
                return src.substring(0, MAX_DLZKA);
            }
        }
    }
    
    /**
     * @return the platny
     */
    public boolean isPlatny() {
        return platny;
    }

    /**
     * @param platny the platny to set
     */
    public void setPlatny(boolean platny) {
        this.platny = platny;
    }
}
